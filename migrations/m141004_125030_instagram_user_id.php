<?php

use yii\db\Schema;
use yii\db\Migration;

class m141004_125030_instagram_user_id extends Migration
{
    public function up()
    {
        $this->addColumn('instagram_user', 'instagram_id', 'integer');
    }

    public function down()
    {
        echo "m141004_125030_instagram_user_id cannot be reverted.\n";

        return false;
    }
}
