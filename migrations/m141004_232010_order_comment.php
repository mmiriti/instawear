<?php

use yii\db\Schema;
use yii\db\Migration;

class m141004_232010_order_comment extends Migration
{
    public function up()
    {
        $this->addColumn('order', 'comment', 'text');
    }

    public function down()
    {
        echo "m141004_232010_order_comment cannot be reverted.\n";

        return false;
    }
}
