<?php

use yii\db\Schema;
use yii\db\Migration;

class m141004_113727_instagram_user extends Migration
{
    public function up()
    {
        $this->createTable('instagram_user', [
            'id' => 'pk',
            'create_time' => 'timestamp default current_timestamp',
            'username' => 'string',
            'access_token' => 'string',
        ], 'engine=InnoDB');
    }

    public function down()
    {
        echo "m141004_113727_instagram_user cannot be reverted.\n";

        return false;
    }
}
