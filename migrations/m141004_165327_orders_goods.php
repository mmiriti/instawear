<?php

use yii\db\Schema;
use yii\db\Migration;

class m141004_165327_orders_goods extends Migration
{
    public function up()
    {
        $this->createTable('good', [
            'id' => 'pk',
            'name' => 'string',
            'old_price' => 'money',
            'price' => 'money',
            'type' => "enum('men', 'women', 'kids')",
            'sizes' => 'text',
        ], 'engine=InnoDB');

        $this->createTable('good_photo', [
            'id' => 'pk',
            'good_id' => 'int',
            'create_time' => 'timestamp default current_timestamp',
            'file' => 'string',
            'title' => 'string',
            'order' => 'int',
        ], 'engine=InnoDB');

        $this->addForeignKey('fk_good_photo_good', 'good_photo', 'good_id', 'good', 'id');

        $this->createTable('order', [
            'id' => 'pk',
            'create_time' => 'timestamp default current_timestamp',
            'good_id' => 'int',
            'user_id' => 'int',
            'size' => 'string',
            'name' => 'string',
            'phone' => 'string',
        ], 'engine=InnoDB');

        $this->addForeignKey('fk_order_good', 'order', 'good_id', 'good', 'id');
        $this->addForeignKey('fk_order_user', 'order', 'user_id', 'instagram_user', 'id');

        $this->createTable('order_photo', [
            'id' => 'pk',
            'order_id' => 'int',
            'instagram_id' => 'string',
            'url' => 'string',
        ], 'engine=InnoDB');

        $this->addForeignKey('fk_order_photo_order', 'order_photo', 'order_id', 'order', 'id');
    }

    public function down()
    {
        echo "m141004_165327_orders_goods cannot be reverted.\n";

        return false;
    }
}
