<?php

use yii\db\Schema;
use yii\db\Migration;

class m141006_171409_callbacks extends Migration
{
    public function up()
    {
        $this->createTable('callback', [
            'id' => 'pk',
            'create_time' => 'timestamp default current_timestamp',
            'new' => 'tinyint(1) default 1',
            'name' => 'string',
            'phone' => 'string',
        ], 'engine=InnoDB');
    }

    public function down()
    {
        echo "m141006_171409_callbacks cannot be reverted.\n";

        return false;
    }
}
