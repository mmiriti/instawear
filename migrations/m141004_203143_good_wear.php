<?php

use yii\db\Schema;
use yii\db\Migration;

class m141004_203143_good_wear extends Migration
{
    public function up()
    {
        $this->addColumn('good', 'wear', "enum('t-shirt', 'sweatshirt')");
    }

    public function down()
    {
        echo "m141004_203143_good_wear cannot be reverted.\n";

        return false;
    }
}
