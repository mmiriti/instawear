<?php

use yii\db\Schema;
use yii\db\Migration;

class m141004_221013_order_zip extends Migration
{
    public function up()
    {
        $this->addColumn('order', 'zip_file', 'string');
    }

    public function down()
    {
        echo "m141004_221013_order_zip cannot be reverted.\n";

        return false;
    }
}
