<?php

namespace app\controllers;

use app\models\Good;
use app\models\Order;
use app\models\OrderPhoto;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\HttpException;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Index
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Choose a photo
     *
     */
    public function actionChoose($user_id = null, $max_id = null, $season = null)
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect(['site/index']);
        } else {
            if ($user_id == null) {
                $user_id = Yii::$app->user->identity->instagram_id;
            }

            if ($season == null) {
                $data = Yii::$app->instagram->userMediaRecent($user_id, 20, $max_id);
            } else {
                $months = [];
                switch ($season) {
                    case 'fall':
                        $months = [9, 10, 11];
                        break;
                    case 'winter':
                        $months = [12, 1, 2];
                        break;

                    case 'spring':
                        $months = [3, 4, 5];
                        break;
                    case 'summer':
                        $months = [6, 7, 8];
                        break;
                }
                $data = Yii::$app->instagram->userMediaForMonths($user_id, $months);
            }

            if (isset($data['pagination']) && isset($data['pagination']['next_max_id'])) {
                $max_id = $data['pagination']['next_max_id'];
            } else {
                $max_id = null;
            }

            if (Yii::$app->request->isAjax) {
                return $this->renderPartial('_photos', ['photos' => $data['data'], 'max_id' => $max_id, 'user_id' => $user_id]);
            } else {
                return $this->render('choose', ['data' => $data, 'user_id' => $user_id, 'max_id' => $max_id]);
            }
        }
    }

    /**
     * Choose a wear
     *
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionWear($id = null)
    {
        if ($id == null) {
            $order = new Order();
            $order->user_id = Yii::$app->user->identity->id;

            if ($order->save()) {
                if (isset($_POST['photo'])) {
                    foreach ($_POST['photo'] as $instagram_id) {
                        $orderPhoto = new OrderPhoto();
                        $orderPhoto->instagram_id = $instagram_id;
                        $orderPhoto->order_id = $order->id;
                        if (!$orderPhoto->save()) {
                            throw new HttpException(500);
                        }
                    }
                    $this->redirect(['wear', 'id' => $order->id]);
                } else {
                    throw new HttpException(400);
                }
            } else {
                throw new HttpException(500);
            }
        } else {
            $order = Order::find()->where(['id' => $id])->one();
            if ($order != null) {
                if (Yii::$app->request->isPost) {
                    if ($order->load(Yii::$app->request->post())) {
                        $good = Good::find()->where(['type' => $_POST['Good']['type'], 'wear' => $_POST['Good']['wear']])->one();
                        if ($good != null) {
                            $order->good_id = $good->id;
                            if ($order->save()) {
                                $this->redirect(['site/form', 'id' => $order->id]);
                            }
                        } else {
                            throw new HttpException(500, 'Нельзя оформить заказ на несуществующий товар');
                        }
                    }
                }
            } else {
                throw new HttpException(404);
            }
        }
        return $this->render('wear', ['order' => $order]);
    }

    public function actionWearsizes($type, $wear)
    {
        $wear = Good::find()->where(['type' => $type, 'wear' => $wear])->one();
        return $this->renderPartial('wearsizes', ['good' => $wear]);
    }

    public function actionWearphotos($type, $wear)
    {
        $wear = Good::find()->where(['type' => $type, 'wear' => $wear])->one();
        return $this->renderPartial('wearphotos', ['good' => $wear]);
    }

    public function actionWearprices($type, $wear)
    {
        $wear = Good::find()->where(['type' => $type, 'wear' => $wear])->one();
        return $this->renderPartial('wearprices', ['good' => $wear]);
    }

    public function actionForm($id)
    {
        $order = Order::find()->where(['id' => $id])->one();

        if (($order->load(Yii::$app->request->post())) && ($order->save())) {
            $this->redirect(['site/thankyou']);
        }

        if ($order != null) {
            return $this->render('form', ['order' => $order]);
        } else {
            throw new HttpException(404);
        }
    }

    public function actionThankyou()
    {
        return $this->render('thankyou');
    }

    public function actionUpload($id = null, $delete_photo = null)
    {
        if ($delete_photo != null) {
            $photo = OrderPhoto::find()->where(['id' => $delete_photo])->one();
            if ($photo != null) {
                $photo->delete();
            }
            if (Yii::$app->request->isAjax) {
                return Json::encode(['status' => 'success']);
            } else {
                $this->redirect(['upload', 'id' => $id]);
            }
        }
        if ($id != null) {
            $order = Order::find()->where(['id' => $id])->one();
            if ($order == null) {
                throw new HttpException(404);
            }
        } else {
            $order = new Order();
        }

        if (Yii::$app->request->isPost) {
            $order->load(Yii::$app->request->post());
            if ($order->save()) {
                foreach ($_FILES['files']['error'] as $index => $value) {
                    if ($value == 0) {
                        $parts = explode('.', $_FILES['files']['name'][$index]);

                        $file_name = 'upload/n_' . time() . '_' . mt_rand(100000, 999999) . '.' . array_pop($parts);

                        if (move_uploaded_file($_FILES['files']['tmp_name'][$index], $file_name)) {
                            $orderPhoto = new OrderPhoto();
                            $orderPhoto->order_id = $order->id;
                            $orderPhoto->url = $file_name;
                            $orderPhoto->save();
                        }

                        if ($order->photoCount >= 20) {
                            break;
                        }
                    }
                }

                if (isset($_POST['OrderPhoto'])) {
                    foreach ($_POST['OrderPhoto'] as $id) {
                        $photo = OrderPhoto::find()->where(['id' => $id])->one();
                        $photo->order_id = $order->id;
                        $photo->save();
                    }
                }

                $this->redirect(['upload', 'id' => $order->id]);
            } else {
                throw new HttpException(500);
            }
        }
        return $this->render('upload', ['order' => $order]);
    }

    /**
     * @return string
     */
    public function actionUploadimage()
    {
        if ($_FILES['image']['error'] == 0) {
            $parts = explode('.', $_FILES['image']['name']);

            $file_name = 'upload/n_' . time() . '_' . mt_rand(100000, 999999) . '.' . array_pop($parts);

            if (move_uploaded_file($_FILES['image']['tmp_name'], $file_name)) {
                $orderPhoto = new OrderPhoto();
                $orderPhoto->url = $file_name;
                if ($orderPhoto->save()) {
                    $data['id'] = $orderPhoto->id;
                    $data['url'] = Yii::getAlias('@web') . '/' . $file_name;
                    return Json::encode($data);
                }
            }
        }
    }

    /**
     * Logout
     *
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
