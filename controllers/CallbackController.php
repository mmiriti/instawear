<?php

namespace app\controllers;

use app\models\Callback;
use Yii;

class CallbackController extends \yii\web\Controller
{
    public function actionCreate()
    {
        $callback = new Callback();

        if (Yii::$app->request->isPost && $callback->load(Yii::$app->request->post())) {
            if ($callback->save()) {
                $this->redirect(['success']);
            }
        }
        return $this->render('create', ['callback' => $callback]);
    }

    public function actionSuccess()
    {
        return $this->render('success');
    }

}
