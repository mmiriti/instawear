<?php

namespace app\controllers;

use app\models\InstagramUser;
use Yii;
use app\models\User;
use app\components\Instagram;
use yii\helpers\Json;
use yii\web\HttpException;

class OauthController extends \yii\web\Controller
{
    public function actionIndex($code = null, $error = null, $error_reason = null, $error_description = null)
    {
        if ($code == null) {
            throw new CHttpException(500, $error_description);
        } else {
            /* @var Yii::$app->instagram Instagram */
            $postData = array(
                'client_id' => Yii::$app->instagram->client_id,
                'client_secret' => Yii::$app->instagram->client_secret,
                'grant_type' => 'authorization_code',
                'redirect_uri' => Yii::$app->instagram->redirect_uri,
                'code' => $code,
            );

            $curl = curl_init('https://api.instagram.com/oauth/access_token');
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 0);

            $curlResult = curl_exec($curl);

            if ($curlResult !== false) {
                $data = Json::decode($curlResult, true);

                if (isset($data['access_token'])) {
                    $user = InstagramUser::find()->where(['username' => $data['user']['username']])->one();

                    if ($user == null) {
                        $user = new InstagramUser();
                        $user->instagram_id = $data['user']['id'];
                        $user->access_token = $data['access_token'];
                        $user->username = $data['user']['username'];
                        $user->save();
                    }

                    if (Yii::$app->user->login($user, 3600 * 24 * 30)) {
                        $this->redirect(['site/choose']);
                    } else {
                        throw new HttpException(500, 'не удалось авторизироваться с использованием Instagram');
                    }
                }
            }
        }
    }

    public function actionTest($username)
    {
        $user = InstagramUser::find()->where(['username' => $username])->one();
        if (Yii::$app->user->login($user, 3600 * 24 * 30)) {
            $this->redirect(['site/choose']);
        }
    }
}
