<?php
/**
 * Created by PhpStorm.
 * User: michaelmiriti
 * Date: 04/10/14
 * Time: 00:38
 */

namespace app\components;

use Yii;
use yii\base\Component;
use yii\helpers\Json;
use yii\web\HttpException;

/**
 * Class Instagram
 * @package app\components
 *
 * @property string $authUrl
 */
class Instagram extends Component
{
    public $api_url = 'https://api.instagram.com/v1/';

    public $client_id;
    public $client_secret;
    public $website_url;
    public $redirect_uri;

    protected $dump_urls = false;

    /**
     * Запрос к Instagram API
     *
     * @param $method
     * @param array $params
     * @return mixed
     * @throws \yii\web\HttpException
     */
    public function request($method, $params = [])
    {
        if (!Yii::$app->user->isGuest) {
            $params['access_token'] = Yii::$app->user->identity->getAttribute('access_token');

            $endpoint_url = $this->api_url . $method . '?' . http_build_query($params);

            if ($this->dump_urls) {
                echo $endpoint_url . "\n";
            }

            $curl = curl_init($endpoint_url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
            $current = microtime(true);
            $data = curl_exec($curl);
            Yii::info('Instagram API request ' . $endpoint_url . ' (' . (microtime(true) - $current) . ')');

            if ($data !== false) {
                return Json::decode($data);
            } else {
                throw new HttpException(500, 'Ошибка при обращении к InstagramAPI: ' . curl_error($curl));
            }
        } else {
            throw new HttpException(500, 'Попытка обращения к API неавторизированным пользователем');
        }
    }

    /**
     * Self feed
     *
     * @return mixed
     */
    public function userSelfFeed($max_id = null)
    {
        $params = $max_id == null ? [] : ['max_id' => $max_id];
        return $this->request('users/self/feed', $params);
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public function userMediaRecent($user_id, $count = 50, $max_id = null)
    {
        $params = ['count' => $count];
        if ($max_id != null) {
            $params['max_id'] = $max_id;
        }
        return $this->request('users/' . $user_id . '/media/recent/', $params);
    }

    /**
     * @param $user_id
     * @param array $months
     * @return null
     */
    public function userMediaForMonths($user_id, $months = [])
    {
        if (count($months) > 0) {
            $params = ['count' => -1];
            $data = [];
            do {
                $recent = $this->request('users/' . $user_id . '/media/recent/', $params);
                foreach ($recent['data'] as $media) {
                    $media_year = intval(date("Y", $media['created_time']));
                    $media_month = intval(date("m", $media['created_time']));
                    if (($media_year < date("Y")) && ($media_month < date("m"))) {
                        break;
                    }

                    if (in_array($media_month, $months)) {
                        $data[] = $media;
                    }
                }
                if ((isset($recent['pagination'])) && (isset($recent['pagination']['next_max_id']))) {
                    $params['max_id'] = $recent['pagination']['next_max_id'];
                } else {
                    break;
                }
            } while (1);

            return [
                'data' => $data
            ];
        } else {
            return null;
        }
    }

    /**
     * @param null $user_id
     * @return mixed|null
     */
    public function userFollows($user_id = null)
    {
        if ($user_id == null) {
            $user_id = Yii::$app->user->identity->instagram_id;
        }

        $data = null;
        $cursor = null;
        do {
            $params = [];
            if ($cursor != null) {
                $params['cursor'] = $cursor;
            }
            $new_data = $this->request('users/' . $user_id . '/follows', $params);
            if (isset($new_data['pagination']['next_cursor']))
                $cursor = $new_data['pagination']['next_cursor'];
            if ($data == null) {
                $data = $new_data;
            } else {
                $data['data'] = array_merge($data['data'], $new_data['data']);
            }

        } while (isset($new_data['pagination']['next_cursor']));

        return $data;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function media($id)
    {
        return $this->request('media/' . $id);
    }

    public function user($id)
    {
        return $this->request('users/' . $id . '/');
    }

    /**
     * Путь к авторизации
     *
     * @return string
     */
    public function getAuthUrl()
    {
        return 'https://api.instagram.com/oauth/authorize/?client_id=' . $this->client_id . '&redirect_uri=' . $this->redirect_uri . '&response_type=code';
    }
} 