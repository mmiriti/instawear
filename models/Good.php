<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "good".
 *
 * @property integer $id
 * @property string $name
 * @property string $old_price
 * @property string $price
 * @property string $type
 * @property string $sizes
 * @property string $wear
 *
 * @property array $sizesArray
 *
 * @property GoodPhoto[] $goodPhotos
 * @property Order[] $orders
 */
class Good extends \yii\db\ActiveRecord
{
    public function getSizesArray()
    {
        $data = explode("\n", strtolower($this->sizes));;
        $result = array();
        foreach ($data as $size) {
            $result[] = trim($size);
        }
        return $result;
    }

    public function delete()
    {
        foreach ($this->goodPhotos as $photo) {
            $photo->delete();
        }
        return parent::delete();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'good';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['old_price', 'price'], 'number'],
            [['type', 'sizes', 'wear'], 'string'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название товара',
            'old_price' => 'Старая цена',
            'price' => 'Цена',
            'type' => 'Тип',
            'sizes' => 'Размеры',
            'wear' => 'Вид одежды',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodPhotos()
    {
        return $this->hasMany(GoodPhoto::className(), ['good_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['good_id' => 'id']);
    }
}
