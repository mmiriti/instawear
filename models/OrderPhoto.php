<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_photo".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $instagram_id
 * @property string $url
 *
 * @property Order $order
 */
class OrderPhoto extends \yii\db\ActiveRecord
{
    public function delete()
    {
        if ($this->url != null) {
            @unlink($this->url);
        }
        return parent::delete();
    }

    public function download()
    {
        if ($this->url == null) {
            $data = Yii::$app->instagram->media($this->instagram_id);

            $link = $data['data']['images']['standard_resolution']['url'];

            $local_image_file_name = 'upload/dw_' . time() . '_' . mt_rand(100000, 999999) . '.jpg';
            if (file_put_contents($local_image_file_name, fopen($link, "r"))) {
                $this->url = $local_image_file_name;
                $this->save();
            }
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_photo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id'], 'integer'],
            [['instagram_id', 'url'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'instagram_id' => 'Instagram ID',
            'url' => 'Url',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
