<?php

namespace app\models;

use Yii;
use yii\web\HttpException;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property string $create_time
 * @property integer $good_id
 * @property integer $user_id
 * @property string $size
 * @property string $name
 * @property string $phone
 * @property string $zip_file
 * @property integer $photoCount
 * @property string $comment
 *
 * @property InstagramUser $user
 * @property Good $good
 * @property OrderPhoto[] $orderPhotos
 */
class Order extends \yii\db\ActiveRecord
{
    public function downloadAll()
    {
        if ($this->zip_file == null) {
            $zip_file = 'upload/order-' . $this->id . '.zip';
            $zip = new \ZipArchive();
            if ($zip->open($zip_file, \ZipArchive::CREATE) === TRUE) {

                foreach ($this->orderPhotos as $photo) {
                    $photo->download();
                    $zip->addFile($photo->url);
                }

                $zip->close();

                $this->zip_file = $zip_file;
                $this->save();
            } else {
                throw new HttpException(500, 'Не удалось создать архив');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['create_time', 'zip_file', 'comment'], 'safe'],
            [['good_id', 'user_id'], 'integer'],
            [['size', 'name', 'phone'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_time' => 'Дата и время создания',
            'good_id' => 'Товар',
            'user_id' => 'Пользователь',
            'size' => 'Размер',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'zip_file' => 'Zip архив',
            'photoCount' => 'Кол-во фотографиий',
            'comment' => 'Комментарий',
        ];
    }

    public function getPhotoCount()
    {
        return $this->hasMany(OrderPhoto::className(), ['order_id' => 'id'])->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(InstagramUser::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGood()
    {
        return $this->hasOne(Good::className(), ['id' => 'good_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderPhotos()
    {
        return $this->hasMany(OrderPhoto::className(), ['order_id' => 'id']);
    }
}
