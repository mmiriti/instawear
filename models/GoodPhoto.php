<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "good_photo".
 *
 * @property integer $id
 * @property integer $good_id
 * @property string $create_time
 * @property string $file
 * @property string $title
 * @property integer $order
 *
 * @property Good $good
 */
class GoodPhoto extends \yii\db\ActiveRecord
{
    public function delete()
    {
        @unlink($this->file);
        return parent::delete();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'good_photo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['good_id', 'order'], 'integer'],
            [['create_time'], 'safe'],
            [['file'], 'file'],
            [['file'], 'required'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'good_id' => 'Good ID',
            'create_time' => 'Create Time',
            'file' => 'Файл',
            'title' => 'Заголовок',
            'order' => 'Порядок',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGood()
    {
        return $this->hasOne(Good::className(), ['id' => 'good_id']);
    }
}
