<?php
/**
 * Created by PhpStorm.
 * User: michaelmiriti
 * Date: 04/10/14
 * Time: 21:59
 */

namespace app\modules\admin;


use yii\web\Controller;

class AdminController extends Controller
{
    public $layout = 'admin';
} 