<?php

namespace app\modules\admin\controllers;

use app\models\GoodPhoto;
use Yii;
use app\models\Good;
use yii\data\ActiveDataProvider;
use app\modules\admin\AdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * GoodsController implements the CRUD actions for Good model.
 */
class GoodsController extends AdminController
{
    /**
     * Lists all Good models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Good::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Good model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Good model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Good();
        $newPhoto = new GoodPhoto();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                $this->uploadPhoto($model, $newPhoto);
            }
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'newPhoto' => $newPhoto,
            ]);
        }
    }

    /**
     * Updates an existing Good model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $delete_photo = null)
    {
        if ($delete_photo != null) {
            $photo = GoodPhoto::find()->where(['id' => $delete_photo])->one();
            $photo->delete();
            $this->redirect(['update', 'id' => $id]);
        }

        $model = $this->findModel($id);
        $newPhoto = new GoodPhoto();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                $this->uploadPhoto($model, $newPhoto);
                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'newPhoto' => $newPhoto,
            ]);
        }
    }

    /**
     * Deletes an existing Good model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Good model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Good the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Good::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function uploadPhoto($good, $photo)
    {
        $photo->load(Yii::$app->request->post());
        $photo->good_id = $good->id;
        $photo->file = UploadedFile::getInstance($photo, 'file');
        if ($photo->validate()) {
            $file_name = time() . '_' . mt_rand(100000, 999999) . '.' . $photo->file->extension;
            if ($photo->file->saveAs('upload/' . $file_name)) {
                $photo->file = 'upload/' . $file_name;
                $photo->save();
            }
        }
    }
}
