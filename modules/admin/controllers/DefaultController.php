<?php

namespace app\modules\admin\controllers;

use app\modules\admin\AdminController;

class DefaultController extends AdminController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}
