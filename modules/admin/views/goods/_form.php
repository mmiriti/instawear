<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Good */
/* @var $form yii\widgets\ActiveForm */
/* @var $newPhoto \app\models\GoodPhoto */
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<div class="good-form panel panel-default">
    <div class="panel-body">

        <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

        <?= $form->field($model, 'old_price')->textInput(['maxlength' => 19]) ?>

        <?= $form->field($model, 'price')->textInput(['maxlength' => 19]) ?>

        <?= $form->field($model, 'type')->dropDownList(['men' => 'Мужское', 'women' => 'Женское', 'kids' => 'Детское',], ['prompt' => '']) ?>

        <?= $form->field($model, 'wear')->dropDownList(['t-shirt' => 'Футболка', 'sweatshirt' => 'Свитшот',], ['prompt' => '']) ?>

        <?= $form->field($model, 'sizes')->textarea(['rows' => 6]) ?>

        <div class="well">
            <?php
            if (count($model->goodPhotos) > 0) {
                ?>
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>Фото</th>
                        <th>Заголовок</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($model->goodPhotos as $photo) {
                        ?>
                        <tr>
                            <td><img src="<?= Yii::getAlias('@web') . '/' . $photo->file ?>" style="height: 50px;"
                                     alt="">
                            </td>
                            <td><?= $photo->title; ?></td>
                            <td>
                                <?= Html::a('удалить', ['update', 'id' => $model->id, 'delete_photo' => $photo->id]); ?>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                    </tbody>
                </table>
            <?php
            }
            ?>
            <div class="form-group">
                <label for="">Загрузить фото</label>
                <?= $form->field($newPhoto, 'file')->fileInput(['class' => 'form-control']) ?>
                <?= $form->field($newPhoto, 'title')->textInput() ?>
            </div>
        </div>

    </div>
    <div class="panel-footer">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

</div>
<?php ActiveForm::end(); ?>
