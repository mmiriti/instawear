<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Good */
/* @var $newPhoto \app\models\GoodPhoto */

$this->title = 'Изменить товар: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="good-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'newPhoto' => $newPhoto,
    ]) ?>

</div>
