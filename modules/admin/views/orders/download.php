<?php
/* @var $order \app\models\Order */
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Скачать изоборажения</h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h2><a href="<?= Yii::getAlias('@web') . '/' . $order->zip_file ?>"><span
                            class="glyphicon glyphicon-save"></span> Скачать zip-архив</a></h2>
            </div>
        </div>
        <div class="row">
            <?php
            foreach ($order->orderPhotos as $photo) {
                ?>
                <div class="col-md-2">
                    <img src="<?= Yii::getAlias('@web') . '/' . $photo->url ?>" alt="" class="thumbnail img-responsive">
                </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>