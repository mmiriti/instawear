<?php

use yii\bootstrap\Progress;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;

$loadingModal = Modal::begin([
    'header' => 'Загрузка..',
    'closeButton' => false,
    'options' => ['data-backdrop' => 'static', 'data-keyboard' => false],
]);
echo Progress::widget([
    'percent' => 100,
    'options' => ['class' => 'active progress-striped']
]);
echo "Идет скачивание фотографий из Instagram и формирование ZIP архива. Это может занять до нескольких минут в зависимости от количества фотографий и качества канала связи между площадкой сайта и сервером Instagram.";
Modal::end();
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'create_time',
            'good.name',
            'size',
            'name',
            'phone',
            'photoCount',
            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'download' => function ($url, $model, $key) {
                            /* @var $model \app\models\Order */
                            if ($model->photoCount > 0) {
                                return Html::a('<span class="glyphicon glyphicon-save"></span>', $url, ['class' => $model->zip_file == null ? 'interactive-load' : '']);
                            } else {
                                return '';
                            }
                        }
                ],
                'template' => '{view} {delete} {download}'
            ],
        ],
    ]); ?>

</div>
<script>
    $(function () {
        $('.interactive-load').on('click', function (e) {
            e.preventDefault();

            var url = $(this).attr('href');

            $('#<?= $loadingModal->id; ?>').modal();

            $.ajax({
                url: url,
                success: function () {
                    window.location.href = url;
                },
                error: function () {
                    $('#<?= $loadingModal->id; ?>').modal('hide');
                    alert('Во время формирования архива произошла ошибка. Скорее всего операция не смогла завершиться за резрешенное сервером время (<?= ini_get('max_execution_time') ?> сек.). Попробуйте еще раз, операция продолжится с момента разрыва. Воизбежание повторения этой ошибки рекомендуется увеличить максимальное время выполнения в настройках PHP');
                }
            });
        });
    });
</script>