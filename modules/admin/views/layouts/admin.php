<?php
/* @var $this \yii\base\View */
/* @var $content string */

use \app\assets\AppAsset;

use yii\bootstrap\NavBar;

AppAsset::register($this);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $this->title ?></title>

    <!-- Bootstrap -->
    <link href="<?= Yii::getAlias('@web') ?>/css/bootstrap.min.css" rel="stylesheet">

    <script src="<?= Yii::getAlias('@web') ?>/js/jquery.min.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/bootstrap.min.js"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php
$count = \app\models\Callback::find()->count();
NavBar::begin(['brandLabel' => 'Админка']);
echo \yii\bootstrap\Nav::widget([
    'items' => [
        ['label' => 'Заказы', 'url' => ['orders/index']],
        ['label' => 'Товары', 'url' => ['goods/index']],
        ['label' => 'Обратные звонки <span class="badge">' . $count . '</span>', 'url' => ['callbacks/index']],
    ],
    'encodeLabels' => false,
    'options' => ['class' => 'nav navbar-nav'],
]);
NavBar::end();
?>
<div class="container">
    <?= $content ?>
</div>
</body>
</html>