<?php
/* @var $callback \app\models\Callback */
?>
<main>
    <section id="pattern-background-1" class="light-bg img-bg-softer">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="alert alert-success" style="margin-top: 30px;margin-bottom: 500px;">
                        Спасибо! Скоро мы перезвонима вам!
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>