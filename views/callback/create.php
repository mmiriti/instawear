<?php
use app\models\Callback;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Button;

/* @var $this yii\web\View */
/* @var $callback Callback */

?>
<main>
    <section id="pattern-background-1" class="light-bg img-bg-softer">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1" style="padding-top: 50px;">
                    <h1>Заказать обратный звонок</h1>
                    <?php
                    $form = ActiveForm::begin();
                    echo $form->field($callback, 'name')->textInput();
                    echo $form->field($callback, 'phone')->textInput();
                    echo Button::widget([
                        'label' => 'Отправить',
                    ]);
                    ActiveForm::end();
                    ?>
                </div>
            </div>
        </div>
    </section>

</main>