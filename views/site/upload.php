<?php
use yii\helpers\Html;
use yii\base\View;
use app\models\Order;
use yii\helpers\Url;

/* @var $order Order */
/* @var $this View */

?>
<style>
    .files-drop {
        height: 200px;
        border: 3px dashed #000000;
        background-color: #afd9ee;
        border-radius: 5px;
        text-align: center;
        padding-top: 80px;
        font-size: 40px;
    }

    .files-drop.drag-hover {
        background-color: #269abc;
    }

    .thumbnail {
        margin-bottom: 0px;
        max-height: 120px;
    }

    .image-item {
        min-height: 150px;
    }

    .image-item .glyphicon {
        margin-bottom: 10px;
    }
</style>

<script>
    totalPhoto = 0;
    $(function () {
        $('.files-drop').on('dragover', function (e) {
            $(this).addClass('drag-hover');
            e.preventDefault();
            e.stopPropagation();
        });

        $('.files-drop').on('dragleave', function (e) {
            $(this).removeClass('drag-hover');
            e.preventDefault();
            e.stopPropagation();
        });

        $('.files-drop').on('dragover', function (e) {
            e.preventDefault();
            e.stopPropagation();
        });

        $('.files-drop').on('dragenter', function (e) {
            e.preventDefault();
            e.stopPropagation();
        });

        var uploadInProgress = false;

        function updateGallery() {

        }

        $('.files-drop').bind('drop', function (e) {
            if (uploadInProgress) return;
            if (e.originalEvent.dataTransfer) {
                if (e.originalEvent.dataTransfer.files.length) {
                    e.preventDefault();
                    e.stopPropagation();

                    if (totalPhoto >= 30) {
                        alert('Нельзя загрузить больше 30 фото!');
                        return;
                    }

                    uploadInProgress = true;

                    $('.files-drop').html('Загрузка...');

                    var filesToLoad = e.originalEvent.dataTransfer.files.length;
                    var filesLoaded = 0;
                    var filesQueue = [];

                    for (var i = e.originalEvent.dataTransfer.files.length - 1; i >= 0; i--) {
                        var file = e.originalEvent.dataTransfer.files[i];
                        filesQueue.push(file);
                    }

                    var fileNum = 0;

                    var loadNextFile = function () {
                        if (fileNum >= filesQueue.length) {
                            uploadInProgress = false;
                            $('.files-drop').html('Готово!');
                            $('.files-drop').removeClass('drag-hover');
                            return;
                        }
                        $('.files-drop').html('Загрузка ' + filesQueue[fileNum].name);
                        var xhr = new XMLHttpRequest();

                        xhr.onreadystatechange = function (e) {
                            if (e.target.readyState == 4) {
                                if (e.target.status == 200) {
                                    filesLoaded++;

                                    console.log(this.responseText);

                                    var obj = JSON.parse(this.responseText);
                                    var $el = $('<div/>', {
                                        'class': 'col-md-2 image-item',
                                        'id': 'photoContainer-' + obj.id
                                    });

                                    var $image = $('<img/>', {
                                        'src': obj.url,
                                        'class': 'thumbnail img-responsive'
                                    });

                                    var $hidden = $('<input/>', {
                                        'type': 'hidden',
                                        'name': 'OrderPhoto[]',
                                        'value': obj.id
                                    });

                                    var $link = $('<a/>', {
                                        'href': '<?= Url::to(['upload']) ?>?delete_photo=' + obj.id,
                                        'class': 'delete-photo',
                                        'data-photo-id': obj.id,
                                        'title': 'Удалить фотографию'
                                    });

                                    $link.append(
                                        $('<span/>', {
                                            'class': 'glyphicon glyphicon-remove'
                                        })
                                    );

                                    $el.append($image);
                                    $el.append($hidden);
                                    $el.append($link);

                                    $('#imagesContainer').append($el);

                                    totalPhoto++;

                                    $('#photoCount').html(totalPhoto);

                                    deleteEvent();

                                    fileNum++;
                                    loadNextFile();
                                }
                            }
                        };

                        xhr.open('POST', '<?= Url::to(['site/uploadimage']) ?>');

                        var fd = new FormData();
                        fd.append("image", filesQueue[fileNum]);
                        xhr.send(fd);
                    }

                    loadNextFile();
                }
            }
        });

        var deleteEvent = function () {
            $('.delete-photo').on('click', function (e) {
                e.preventDefault();

                var $this = $(this);
                $this.hide();
                var photoID = $(this).attr('data-photo-id');

                totalPhoto--;
                $('#photoCount').html(totalPhoto);
                if (totalPhoto < 9) {
                    $('#doneButton').addClass('disabled');
                }

                $.ajax({
                    url: '<?= Url::to(['upload']) ?>?delete_photo=' + photoID,
                    'dataType': 'json',
                    success: function (jData) {
                        $('#photoContainer-' + photoID).remove();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $this.show();
                        totalPhoto++;
                        $('#photoCount').html(totalPhoto);
                        if (totalPhoto >= 9) {
                            $('#doneButton').removeClass('disabled');
                        }
                    }
                });
            });
        };
        deleteEvent();

    });
</script>

<main>
    <section>
        <div class="container">
            <h1>Загрузка фото без Instagram</h1>

            <h2>Загруженно <span id="photoCount"><?= $order->photoCount; ?></span></h2>

            <form action="" method="post" enctype="multipart/form-data">

                <div class="row" id="imagesContainer">
                    <?php
                    if (!$order->isNewRecord) {
                        ?>
                        <script>
                            totalPhoto = <?= $order->photoCount; ?>;
                        </script>
                        <?php
                        foreach ($order->orderPhotos as $photo) {
                            ?>
                            <div class="col-md-2 image-item" id="photoContainer-<?= $photo->id ?>">
                                <input type="hidden" name="OrderPhoto[]" value="<?= $photo->id ?>">
                                <img src="<?= Yii::getAlias('@web') . '/' . $photo->url ?>" alt=""
                                     class="thumbnail img-responsive">
                                <?= Html::a('<span class="glyphicon glyphicon-remove"></span>', ['upload', 'id' => $order->id, 'delete_photo' => $photo->id], ['class' => 'delete-photo', 'data-photo-id' => $photo->id, 'title' => 'Удалить фотографию']); ?>
                            </div>
                        <?php
                        }
                    }
                    ?>
                </div>

                <?php
                if ($order->isNewRecord || $order->photoCount < 20) {
                    ?>
                    <div class="files-drop hidden-xs">
                        Перетащите фотографии сюда
                    </div>
                    <div class="form-group">
                        <label for="files">Выберите файлы</label>
                        <input type="file" accept="image/*" name="files[]" id="files" class="form-control" multiple>
                    </div>
                <?php
                }
                ?>
                <div class="form-group">
                    <label for="comment">Комментарий</label>
                    <?= Html::activeTextarea($order, 'comment', ['class' => 'form-control']); ?>
                </div>
                <div class="form-group">
                    <p>Необходимо загрузить не менее 9 и не более 30 фотографий. После загрузки нажмите "СОХРАНИТЬ" и
                        затем "ПРОДОЛЖИТЬ"</p>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                    <?php
                    echo Html::a('Продолжить', ['site/wear', 'id' => $order->id], ['class' => 'btn btn-primary' . (((!$order->isNewRecord) && ($order->photoCount >= 9)) ? '' : ' disabled'), 'id' => 'doneButton']);
                    ?>
                </div>
            </form>
        </div>
    </section>
</main>