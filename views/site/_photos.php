<?php
use yii\base\View;
use yii\helpers\Url;

/* @var $this View */
/* @var $photos array */
/* @var $max_id string */
/* @var $user_id integer */

foreach ($photos as $media) {
    $photoDate = new DateTime(date("Y-m-d", $media['created_time']));
    $nowDate = new DateTime();

    $diff = $nowDate->diff($photoDate, true);

    $period = '';

    if (($diff->y == 0) && ($diff->m <= 11)) {
        $period = 'year';
    }

    if (($diff->y == 0) && ($diff->m <= 6)) {
        $period .= ' 6mon';
    }

    if ($diff->y >= 1) {
        $period = 'more_year';
    }

    $month = intval(date('m', intval($media['created_time'])));

    $season = '';
    switch ($month) {
        case 1:
        case 2:
        case 12:
            $season = 'winter';
            break;
        case 3:
        case 4:
        case 5:
            $season = 'spring';
            break;
        case 6:
        case 7:
        case 8:
            $season = 'summer';
            break;
        case 9:
        case 10:
        case 11:
            $season = 'fall';
            break;
    }
    ?>
<li class="item thumb <?= $season . ' ' . $period ?>" id="thumb-<?= $media['id'] ?>"
    data-next-url="<?= Url::to(['site/choose', 'user_id' => $user_id, 'max_id' => $max_id]) ?>">
    <figure>
        <div class="icon-overlay icn-link">
            <a href="#" onclick="return photoSelect(this);" data-photo-id="<?= $media['id'] ?>"
               data-likes="<?= $media['likes']['count'] ?>"
               data-selected="0" class="photo-item <?= $season . ' ' . $period ?>">
                <img src="<?= $media['images']['low_resolution']['url']; ?>" alt="" width="280" height="280">
            </a>
        </div>

        <figcaption class="bordered no-top-border">
            <div class="info">
                <h4>@<?= $media['user']['username'] ?></h4>

                <p><span
                        class="glyphicon glyphicon-heart"></span> <?= $media['likes']['count'] ?></p>
            </div>
        </figcaption>
    </figure>
    </li><?php
}
?>