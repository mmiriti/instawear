<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'Авторизируйтесь в Instagram';
?>
<style>
    .info-box {
        background-color: #283645;
        padding: 10px;
        color: #dcf7ff;
    }

    .info-box h2 {
        color: #dcf7ff;
    }

    .inner {
        padding-top: 50px;
    }
</style>
<main>
    <section id="pattern-background-1" class="light-bg img-bg-softer"
             style="background-image: url('<?= Yii::getAlias('@web') ?>/images/art/pattern-background01.jpg');">
        <div class="container">
            <div class="row">
                <div class="col-md-8 inner text-center">
                    <h1>Подключитесь к Instagram</h1>

                    <p>Шаг 1. Войдите в свой инстаграм, чтобы получить доступ к своим фотографиям</p>

                    <p><?= Html::a('Подключиться к Instagram', Yii::$app->instagram->authUrl, ['class' => 'btn btn-primary btn-red']); ?></p>
                </div>
                <div class="col-md-4 inner">
                    <div class="info-box">
                        <h2 class="text-center">- Это безопасно -</h2>

                        <p class="text-center">Все операции проходят через API Instagram. Это значит, что мы не получаем
                            данные от Вашей
                            учетной записи.</p>

                        <p class="text-center">Все загруженные для заказа фотографии мы удаляем в течение суток после
                            выполнения заказа,ну и
                            конечно же никому не передаем и не публикуем.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>