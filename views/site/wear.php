<?php
use app\models\Good;
use yii\bootstrap\Modal;

/* @var $this \yii\base\View */
/* @var $order \app\models\Order */

$this->title = 'Выбор одежды';

$sizeTable = Modal::begin([
    'header' => 'Таблица размеров',
    'options' => ['style' => "z-index: 9999"],
]);

$goods = Good::find()->all();
?>
<div id="sizesContainer"></div>
<?php

Modal::end();
?>
<style>
    #prices {
        margin-bottom: 30px;
        margin-top: 30px;
    }

    #prices .price-new {
        font-size: 40px;
        color: #fd6b62;
        text-align: right;
    }

    #prices .price-old {
        padding-top: 10px;
        text-align: left;
    }

    .updateRadio {
        width: 100%;
    }

    .updateRadio label.btn {
        background-color: #ffffff;
        color: #536a80 !important;
        box-shadow: none;
        border: 2px solid #536a80;
        text-transform: none;
        border-radius: 2px;
    }

    .btn-group > .btn:not(:first-child):not(:last-child):not(.dropdown-toggle) {
        border-right: none;
        border-left: none;
    }

    .btn-group > .btn:first-child {
        border-right: none;
    }

    .btn-group > .btn:last-child {
        border-left: none;
    }

    .updateRadio label..btn:not(:first-child):not(:last-child):not(.dropdown-toggle) {
        border-right: none;
        border-left: none;
    }

    .updateRadio label.btn.active, label.btn:hover {
        background-color: #536a80;
        color: #ffffff !important;
    }
</style>
<main>
    <section id="pattern-background-1" class="light-bg img-bg-softer">
        <div class="container" style="padding-top: 40px">
            <div class="row">
                <div class="col-md-8" id="photos">
                    <!-- placeholder -->
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="btn-group updateRadio" data-toggle="buttons">
                                <label class="btn btn-primary active" style="width: 33%">
                                    <input type="radio" name="type" id="typeMen" value="men" checked> Мужские
                                </label>
                                <label class="btn btn-primary" style="width: 34%">
                                    <input type="radio" name="type" id="typeWomen" value="women"> Женские
                                </label>
                                <label class="btn btn-primary" style="width: 33%">
                                    <input type="radio" name="type" id="typeKids" value="kids"> Детские
                                </label>
                            </div>

                            <div class="btn-group updateRadio" data-toggle="buttons">
                                <label class="btn btn-primary active" style="width: 50%">
                                    <input type="radio" name="wear" id="typeTShirts" value="t-shirt" checked> Футболки
                                </label>
                                <label class="btn btn-primary" style="width: 50%">
                                    <input type="radio" name="wear" id="typeSweatshirt" value="sweatshirt"> Свитшоты
                                </label>
                            </div>

                            <div class="btn-group updateRadio" data-toggle="buttons">
                                <label class="btn btn-primary active" style="width: 20%">
                                    <input type="radio" name="size" id="sizeXS" value="xs" checked> XS
                                </label>
                                <label class="btn btn-primary" style="width: 20%">
                                    <input type="radio" name="size" id="sizeX" value="s"> S
                                </label>
                                <label class="btn btn-primary" style="width: 20%">
                                    <input type="radio" name="size" id="sizeM" value="m"> M
                                </label>
                                <label class="btn btn-primary" style="width: 20%">
                                    <input type="radio" name="size" id="sizeL" value="l"> L
                                </label>
                                <label class="btn btn-primary" style="width: 20%">
                                    <input type="radio" name="size" id="sizeXL" value="xl"> XL
                                </label>
                            </div>

                            <p class="text-center">
                                <a href="#" data-toggle="modal" data-target="#<?= $sizeTable->id; ?>"
                                   style="border-bottom: none; font-size: 15px;"><span
                                        class="glyphicon glyphicon-tags" style="margin-right: 7px;"></span>Таблица
                                    размеров</a>
                            </p>
                        </div>
                    </div>
                    <div class="row" id="prices">
                        <!-- placeholder -->
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-primary btn-block btn-red" id="done">
                                Выбрать и перейти к шагу 4!
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-center">
                                Специально для Вас напечатаем, сошьем и бесплатно отправим за 2 дня.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>

<script>
    var timout = null;

    function updateData() {
        timout = null;

        var type = $('input[name=type]:checked').val();
        var wear = $('input[name=wear]:checked').val();

        $('#photos').load('<?= \yii\helpers\Url::to(['site/wearphotos']) ?>?type=' + type + '&wear=' + wear);
        $('#prices').load('<?= \yii\helpers\Url::to(['site/wearprices']) ?>?type=' + type + '&wear=' + wear);
        $('#sizesContainer').load('<?= \yii\helpers\Url::to(['site/wearsizes']) ?>?type=' + type + '&wear=' + wear);
    }

    $(function () {
        $('.updateRadio').on('mouseup', function () {
            if (timout != null) {
                clearTimeout(timout);
            }
            timout = setTimeout(updateData, 100);
        });

        $('#done').on('click', function (e) {
            var type = $('input[name=type]:checked').val();
            var wear = $('input[name=wear]:checked').val();
            var size = $('input[name=size]:checked').val();

            var form = document.createElement('form');

            form.setAttribute('method', 'post');
            form.setAttribute('action', '<?= \yii\helpers\Url::to(['site/wear', 'id' => $order->id]) ?>');

            var typeInput = document.createElement('input');
            typeInput.setAttribute('type', 'hidden');
            typeInput.setAttribute('value', type);
            typeInput.setAttribute('name', 'Good[type]');
            form.appendChild(typeInput);

            var wearInput = document.createElement('input');
            wearInput.setAttribute('type', 'hidden');
            wearInput.setAttribute('value', wear);
            wearInput.setAttribute('name', 'Good[wear]');
            form.appendChild(wearInput);

            var sizeInput = document.createElement('input');
            sizeInput.setAttribute('type', 'hidden');
            sizeInput.setAttribute('value', size);
            sizeInput.setAttribute('name', 'Order[size]');
            form.appendChild(sizeInput);

            document.body.appendChild(form);

            form.submit();
        });

        updateData();
    });
</script>