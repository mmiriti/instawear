<?php
/* */
?>
<main>
    <section id="pattern-background-1" class="light-bg img-bg-softer"
             style="background-image: url('<?= Yii::getAlias('@web') ?>/images/art/pattern-background01.jpg');">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Спасибо за ваш заказ!</h1>
                </div>
            </div>
        </div>
    </section>
</main>