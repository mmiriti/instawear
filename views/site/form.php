<?php
/* @var $order \app\models\Order */
use yii\helpers\Html;

?>
<main>
    <section id="pattern-background-1" class="light-bg img-bg-softer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Ваши данные</h1>
                    <?php $form = \yii\widgets\ActiveForm::begin(); ?>
                    <?= $form->field($order, 'name')->textInput(); ?>
                    <?= $form->field($order, 'phone')->textInput(); ?>
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']); ?>
                    <?php $form->end(); ?>
                </div>
            </div>
        </div>
    </section>
</main>