<?php
/* @var $name */
/* @var $message */
/* @var $exception */
?>
<main>
    <section id="pattern-background-1" class="light-bg img-bg-softer"
             style="background-image: url(/images/art/pattern-background01.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?= $name; ?></h1>

                    <div class="alert alert-danger">
                        <?= $message ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>