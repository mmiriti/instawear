<?php
/* @var $good \app\models\Good */
?>
<div class="col-md-6">
    <?php
    if ($good != null) {
        ?>
        <h2 class="price-new"><?= number_format($good->price, 0, '.', '&nbsp;'); ?>&nbsp;<i class="fa fa-rub"></i></h2>
    <?php
    } else {
        ?><h2 class="price-new">нет в наличии</h2><?php
    }
    ?>
</div>
<div class="col-md-6">
    <?php
    if ($good != null) {
        if ($good->old_price != null) {
            ?>
            <h2 class="price-old"><span
                    style="text-decoration: line-through;"><?= number_format($good->old_price, 0, '.', '&nbsp;'); ?>
                    &nbsp;<i
                        class="fa fa-rub"></i></span></h2>
        <?php
        }
    } else {
        ?>
        <h2 class="price-old">нет в наличии</h2>
    <?php
    }
    ?>
</div>