<?php

use app\models\Good;

/* @var $good Good */

if ($good != null) {
    if (count($good->goodPhotos) >= 1) {
        ?>
        <div class="row">
            <div class="col-md-12">
                <img id="mainPhoto" class="thumbnail img-responsive"
                     src="<?= Yii::getAlias('@web') . '/' . $good->goodPhotos[0]->file ?>" alt=""
                     style="width: 100%">
            </div>
        </div>
        <?php
        if (count($good->goodPhotos) > 1) {
            ?>
            <div class="row">
                <?php
                foreach ($good->goodPhotos as $photo) {
                    ?>
                    <div class="col-md-3" style="min-height: 200px;">
                        <img style="width: 100%; cursor: hand;"
                             onclick="$('#mainPhoto').attr('src', $(this).attr('src')); return false;"
                             class="thumbnail img-responsive" src="<?= Yii::getAlias('@web') . '/' . $photo->file ?>"
                             alt="">
                    </div>
                <?php
                }
                ?>
            </div>
        <?php
        }
        ?>
    <?php
    } else {
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger">
                    <h1>Нет фото</h1>
                </div>
            </div>
        </div>
    <?php
    }
    ?>
<?php
} else {
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger">
                <h1>Нет в наличии</h1>
            </div>
        </div>
    </div>
<?php
}