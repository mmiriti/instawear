<?php
use \app\controllers\SiteController;
use yii\bootstrap\Modal;
use yii\bootstrap\Progress;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $data array */
/* @var $user_id integer */
/* @var $max_id integer */

$this->title = 'Выберите фото';
?>

<style>
    #bottom-panel {
        background-color: #2f4052;
        position: fixed;
        left: 0;
        bottom: 0;
        right: 0;
        height: 100px;
        color: #fefeff;
    }

    a.photo-item.selected > .icn-more {
        opacity: 0.6;
    }

    .sdropdown {
        position: relative;
        display: inline;
    }

    .sdropdown > a {
        color: #1ABB9C;
        text-decoration: underline;
    }

    .sdropdown ul {
        border: 1px dotted #1ABB9C;
        position: absolute;
        display: none;
        margin: 0;
        padding: 0;
        top: 20px;
        left: -5px;
        z-index: 9999;
        background-color: #ffffff;
    }

    .sdropdown ul li a {
        display: block;
        padding: 0px 5px 0px 5px
    }

    .sdropdown ul li a:hover {
        background-color: #1ABB9C;
        color: #ffffff;
    }

    a.mark-best {
        background-color: #F5F7FA;
        border-radius: 5px;
        padding: 5px 15px;
        margin-left: 5px;
    }

    a.mark-best:hover {
        background-color: #E6E9ED;
    }

    .search-username {
        margin-bottom: 10px;;
    }
</style>

<div class="modal fade" id="followingModal" tabindex="-1" role="dialog" aria-labelledby="followingModalLabel"
     aria-hidden="true" style="z-index: 9999">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Закрыть</span></button>
                <h4 class="modal-title" id="followingModalLabel">Выбрать друга</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form action="">
                            <div class="form-group">
                                <input class="form-control" type="text"
                                       placeholder="Начните вводить имя пользователя" name="username"
                                       id="searchUsername">
                            </div>
                        </form>
                    </div>
                </div>
                <?php
                if ($user_id != Yii::$app->user->identity->instagram_id) {
                    $userData = Yii::$app->instagram->user(Yii::$app->user->identity->instagram_id);
                    ?>
                    <div class="row">
                        <div class="col-md-4 search-username" data-username="<?= $userData['data']['username'] ?>">
                            <a href="<?= Url::to(['choose']) ?>">
                                <img src="<?php echo $userData['data']['profile_picture']; ?>" alt=""
                                     class="img-responsive thumbnail" style="height: 115px; margin-bottom: 0px;">

                                <p>Мой профиль</p>
                            </a>
                        </div>
                    </div>
                <?php
                }
                ?>
                <div class="row">
                    <?php
                    $follows = Yii::$app->instagram->userFollows();

                    foreach ($follows['data'] as $item) {
                        ?>
                        <div class="col-md-3 search-username" data-username="<?= $item['username']; ?>">
                            <a href="<?= Url::to(['choose', 'user_id' => $item['id']]) ?>">
                                <img src="<?php echo $item['profile_picture']; ?>" alt=""
                                     class="img-responsive thumbnail" style="height: 115px; margin-bottom: 0px;">

                                <p>@<?= $item['username']; ?></p>
                            </a>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
            </div>
        </div>
    </div>
</div>

<?php
$loadingModal = Modal::begin([
    'header' => 'Загрузка',
    'closeButton' => false,
    'options' => [
        'data-backdrop' => 'static',
        'data-keyboard' => false,
        'style' => 'z-index: 9999'
    ],
]);
echo 'Подождите, идет загрузка фотографий за нужный период времени.';
echo Progress::widget([
    'percent' => 100,
    'options' => [
        'class' => 'active progress-striped'
    ]
]);
Modal::end();
?>

<section id="portfolio">
    <div class="container inner" style="padding-bottom: 50px; padding-top: 50px;">
        <div class="row">
            <div class="col-md-8 col-sm-9 center-block text-center">
                <header>
                    <h1>Выберите от 9 до 40 фотографий</h1>

                    <p>Вы также можете посмотреть и выбрать из <a href="#followingModal" data-toggle="modal"
                                                                  data-target="#followingModal">аккаунта друга</a></p>

                    Выбрать
                    <div class="sdropdown">
                        <a href="#" data-value="20" id="best-count">20</a>
                        <ul>
                            <li><a href="#" data-value="10">10</a></li>
                            <li><a href="#" data-value="20">20</a></li>
                            <li><a href="#" data-value="30">30</a></li>
                            <li><a href="#" data-value="40">40</a></li>
                        </ul>
                    </div>
                    самых залайканных фотографий за
                    <div class="sdropdown">
                        <a href="#" data-value="summer" id="best-season">лето</a>
                        <ul>
                            <li><a href="#" data-value="fall">осень</a></li>
                            <li><a href="#" data-value="winter">зиму</a></li>
                            <li><a href="#" data-value="spring">весну</a></li>
                            <li><a href="#" data-value="summer">лето</a></li>
                            <li><a href="#" data-value="6mon">пол года</a></li>
                            <li><a href="#" data-value="year">год</a></li>
                        </ul>
                    </div>
                    <a href="#" class="mark-best">Отметить лучше</a>

                </header>
            </div>
        </div>
    </div>

    <div class="container inner-bottom">
        <div class="row">
            <div class="col-sm-12 portfolio">
                <ul class="filter text-center">
                    <li><a href="#" data-filter="*" class="active">Все</a></li>
                    <li><a href="#" data-filter=".selected">Выбранные</a></li>
                    <li><a href="#" data-season="fall" data-filter=".fall">Осень</a></li>
                    <li><a href="#" data-season="summer" data-filter=".summer">Лето</a></li>
                    <li><a href="#" data-season="spring" data-filter=".spring">Весна</a></li>
                    <li><a href="#" data-season="winter" data-filter=".winter">Зима</a></li>
                    <li><a href="#" data-filter=".6mon">Пол года</a></li>
                    <li><a href="#" data-filter=".year">Год</a></li>
                </ul>

                <ul class="items col-4 gap"<?php if ($max_id != null) { ?>
                    data-next-url="<?= Url::to(['site/choose', 'user_id' => $user_id, 'max_id' => $max_id]) ?>" <?php } ?>>
                    <?php echo $this->render('_photos', ['photos' => $data['data'], 'user_id' => $user_id, 'max_id' => $max_id]); ?>
                </ul>

                <p class="text-center">
                    <img src="<?= Yii::getAlias('@web') . '/images/loading.gif' ?>" alt="" style="display: none"
                         id="loading-image">
                </p>
            </div>
        </div>
    </div>
</section>

<div id="bottom-panel">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                Вы выбрали <span id="selectedCount">0</span> <span id="photosRus">фотографий</span> (мин 9, макс 40)
                <button class="btn btn-primary btn-red" id="done" style="margin-left: 10px">Готово! Фотографии выбранны!</button>
            </div>
        </div>
    </div>
</div>

<script>
var selected = [];

function setSelection(el, val) {
    var currentStatus = ($(el).attr('data-selected') == 1);

    if (currentStatus != val) {
        var photoId = $(el).attr('data-photo-id');

        if (val) {
            if (selected.length < 40) {
                $(el).addClass('selected');
                $('#thumb-' + photoId).addClass('selected');
                selected.push(photoId);
            } else {
                alert('Нельзя выбрать больше 40 фотографий!');
            }
        } else {
            $(el).removeClass('selected');
            $('#thumb-' + photoId).removeClass('selected');
            selected.splice(selected.indexOf(photoId), 1);
        }

        $('#selectedCount').html(selected.length);
        $('#photosRus').html(getCorrectEnding(selected.length, ['фотография', 'фотографии', 'фотографий']));

        $(el).attr('data-selected', val ? 1 : 0);
    }
}

function photoSelect(el) {
    var selected = $(el).attr('data-selected');

    setSelection(el, selected == 0);

    return false;
}

function getCorrectEnding(number, endings) {
    number = number % 100;
    if (number >= 11 && number <= 19) {
        return endings[2];
    } else {
        var i = number % 10;
        switch (i) {
            case 1:
                return endings[0];
            case 2:
            case 3:
            case 4:
                return endings[1];
            default:
                return endings[2];
        }
    }
}

function selectMostLiked(count, season) {
    var selector;

    if ((typeof season !== "undefinded") && (season != "")) {
        selector = '.photo-item' + '.' + season;
    } else {
        selector = '.photo-item';
    }

    var items = $(selector);

    var table = [];

    $.each(items, function (i, o) {
        var likesCnt = $(o).attr('data-likes');
        table.push({
            'obj': o,
            'likes': likesCnt
        });

        if (i == items.length - 1) {
            table.sort(function (a, b) {
                return b['likes'] - a['likes'];
            });

            for (var c = 0; c < table.length; c++) {
                if (c < count) {
                    setSelection(table[c].obj, true);
                }
            }
        }
    });

    return false;
}

var loading = false;

function loadPhotos(url, callback) {
    loading = true;
    $('#loading-image').show();
    $.ajax({
        'url': url,
        'success': function (data) {
            var $container = $('.items');
            var $data = $(data);

            if ($data.length > 0) {
                var nextUrl = $($data[0]).attr('data-next-url');
                if (typeof nextUrl !== "undefined") {
                    $container.attr('data-next-url', nextUrl);
                } else {
                    $container.removeAttr('data-next-url');
                }
            } else {
                $container.removeAttr('data-next-url');
            }

            var num = -1;
            var appendNextElement = function () {
                num++;
                if (num < $data.length) {
                    var $el = $($data[num]);
                    if ($el.nodeName != '#text') {
                        var elID = $el.attr('id');
                        if ($('#' + elID).length == 0) {
                            $container.append($el).imagesLoaded(function () {
                                $container.isotope('appended', $el);
                                $('.icn-more').remove();
                                $('.icon-overlay a').prepend('<span class="icn-more"></span>');
                                appendNextElement();
                            });
                        } else {
                            appendNextElement();
                        }
                    } else {
                        appendNextElement();
                    }
                } else {
                    loading = false;
                    $('#loading-image').hide();

                    if (typeof callback !== "undefined") {
                        callback();
                    }
                }
            };

            appendNextElement();
        }
    });
}

function loadMore(callback) {
    var url = $('.items').attr('data-next-url');

    if (typeof url == "undefined") {
        return;
    }

    loadPhotos(url, callback);
}

var loadedSeasons = [];

function loadForSeason(season, callback) {
    if (loadedSeasons.indexOf(season) == -1) {
        $('#<?= $loadingModal->id ?>').modal();
        loadPhotos('<?= Url::to(['site/choose', 'user_id' => $user_id]) ?>&season=' + season, function () {
            loadedSeasons.push(season);
            $('#<?= $loadingModal->id ?>').modal('hide');
            callback();
        });
    } else {
        callback();
    }
}

function loadForYear(callback) {
    if ($('.more_year').length == 0) {
        $('#<?= $loadingModal->id ?>').modal();
        loadMore(function () {
            loadForYear(callback);
        });
    } else {
        $('#<?= $loadingModal->id ?>').modal('hide');
        if (typeof callback != "undefined") {
            callback();
        }
    }
}

$(function () {
    $('.portfolio .filter li a').on('click', (function () {
        var $container = $('.items');

        $('.portfolio .filter li a').removeClass('active');
        $(this).addClass('active');

        var selector = $(this).attr('data-filter');

        if ((selector == '.6mon') || (selector == '.year')) {
            loadForYear(function () {
                $container.isotope({
                    filter: selector
                });
            });
        } else {
            var season = $(this).attr('data-season');

            if (typeof season != "undefined") {
                loadForSeason(season, function () {
                    $container.isotope({
                        filter: selector
                    });
                });
            } else {
                if ((selector == '.selected') && (selected.length == 0)) {
                    alert('Ничего не выбрано');
                    return false;
                }
                $container.isotope({
                    filter: selector
                });
            }
        }

        return false;

    }));

    $('.sdropdown > a').on('click', function (e) {
        e.preventDefault();
        var $ul = $(this).parent().find('ul');
        $ul.toggle();
    });

    $('.sdropdown ul li a').on('click', function (e) {
        var $parent = $(this).parent().parent().hide();
        var $a = $(this).parent().parent().parent().find('>a');
        $parent.hide();
        $a.html($(this).html());
        $a.attr('data-value', $(this).attr('data-value'));
        e.preventDefault();
    });

    $('a.mark-best').on('click', function (e) {
        var season = $('#best-season').attr('data-value');
        loadForSeason(season, function () {
            var count = $('#best-count').attr('data-value');
            selectMostLiked(count, season);
        });

        e.preventDefault();
    });

    $(window).on('scroll', function () {
        if ($(window).scrollTop() >= (($(document).height() - $(window).height()) * 1.00)) {
            if (!loading) {
                loadMore();
            }
        }
    });

    $('#searchUsername').on('keyup', function (e) {
        var $searchUsername = $('.search-username');
        var searchFor = $(this).val();

        if (searchFor == "") {
            $searchUsername.show();
        } else {
            $.each($searchUsername, function (ind, obj) {
                var name = $(obj).attr('data-username');
                if (name.indexOf(searchFor) != -1) {
                    $(obj).show();
                } else {
                    $(obj).hide();
                }
            });
        }
    });

    $('#done').on('click', function (e) {
        if (selected.length >= 9) {
            var form = document.createElement('form');
            form.setAttribute('method', 'post');
            form.setAttribute('action', '<?= \yii\helpers\Url::to(['site/wear']) ?>');

            for (var i = 0; i < selected.length; i++) {
                var f = document.createElement('input');
                f.setAttribute('type', 'hidden');
                f.setAttribute('name', 'photo[]');
                f.setAttribute('value', selected[i]);
                form.appendChild(f);
            }

            document.body.appendChild(form);
            form.submit();
        } else {
            alert('Нужно выбрать как минимум 9 фотографий!');
        }
    });
});
</script>