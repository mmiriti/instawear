<?php
use app\models\Callback;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Button;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>

    <html lang="<?= Yii::$app->language ?>">
    <head>
        <!-- Meta -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <?= Html::csrfMetaTags() ?>

        <title><?= Html::encode($this->title) ?></title>

        <?php $this->head() ?>
        <!-- Bootstrap Core CSS -->
        <link href="<?= Yii::getAlias('@web') ?>/css/bootstrap.min.css" rel="stylesheet">

        <!-- Customizable CSS -->
        <link href="<?= Yii::getAlias('@web') ?>/css/main.css" rel="stylesheet" data-skrollr-stylesheet>
        <link href="<?= Yii::getAlias('@web') ?>/css/green.css" rel="stylesheet" title="Color">
        <link href="<?= Yii::getAlias('@web') ?>/css/owl.carousel.css" rel="stylesheet">
        <link href="<?= Yii::getAlias('@web') ?>/css/owl.transitions.css" rel="stylesheet">
        <link href="<?= Yii::getAlias('@web') ?>/css/animate.min.css" rel="stylesheet">
        <link href="<?= Yii::getAlias('@web') ?>/css/font-awesome.min.css" rel="stylesheet">

        <!-- Fonts -->
        <link href="//fonts.googleapis.com/css?family=Lato:400,900,300,700" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,400italic,700italic"
              rel="stylesheet">

        <!-- Icons/Glyphs -->
        <link href="<?= Yii::getAlias('@web') ?>/fonts/fontello.css" rel="stylesheet">

        <!-- Favicon -->
        <link rel="shortcut icon" href="<?= Yii::getAlias('@web') ?>/images/favicon.ico">

        <script src="<?= Yii::getAlias('@web') ?>/js/jquery.min.js"></script>

        <!-- HTML5 elements and media queries Support for IE8 : HTML5 shim and Respond.js -->
        <!--[if lt IE 9]>
        <script src="<?= Yii::getAlias('@web') ?>/js/html5shiv.js"></script>
        <script src="<?= Yii::getAlias('@web') ?>/js/respond.min.js"></script>
        <![endif]-->

        <style>
            .btn-red {
                background-color: #fd6b62;
            }
        </style>
    </head>

    <body>

    <?php
    $callbackWidget = Modal::begin([
        'header' => 'Закажите обратный звонок',
        'options' => ['style' => 'z-index: 10000']
    ]);

    $newCallback = new Callback();

    $form = ActiveForm::begin(['action' => ['callback/create']]);
    echo $form->field($newCallback, 'name')->textInput();
    echo $form->field($newCallback, 'phone')->textInput();

    echo Button::widget([
        'label' => 'Отправить',
        'options' => ['type' => 'submit']
    ]);

    ActiveForm::end();

    Modal::end();
    ?>

    <header>
        <div class="navbar">

            <div class="navbar-header">
                <div class="container">

                    <ul class="info pull-left">
                        <li><a href="<?= Yii::getAlias('@web') ?>">Уникальная одежда из фотографий с Instagram</a></li>
                    </ul>

                    <ul class="info pull-right">
                        <li><?= Html::a('У меня нет Instagram ;(', ['site/upload']); ?></li>
                    </ul>

                    <a class="navbar-brand" href="/"><img src="<?= Yii::getAlias('@web') ?>/images/logo.svg"></a>

                    <a class="btn responsive-menu pull-right" data-toggle="collapse" data-target=".navbar-collapse"><i
                            class='icon-menu-1'></i></a>

                </div>
            </div>

            <style>
                li.active {
                    font-weight: bold;
                }
            </style>
            <div class="yamm">
                <div class="navbar-collapse collapse">
                    <div class="container">
                        <a class="navbar-brand" href="<?= Yii::getAlias('@web') ?>"><img
                                src="<?= Yii::getAlias('@web') ?>/images/logo.svg"
                                class="logo"
                                alt=""></a>
                        <ul class="nav navbar-nav">
                            <li class="<?= $this->context->action->id == 'index' ? 'active' : '' ?>">
                                <span>1. Вход в Instagram</span>
                            </li>

                            <li class="<?= $this->context->action->id == 'choose' ? 'active' : '' ?>">
                                <span>2. Выбор фото</span>
                            </li>

                            <li class="<?= $this->context->action->id == 'wear' ? 'active' : '' ?>">
                                <span>3. Выбор одежды</span>
                            </li>

                            <li class="<?= $this->context->action->id == 'form' ? 'active' : '' ?>">
                                <span>4. Выши данные</span>
                            </li>

                            <li>
                                <span>= 2,5 минуты</span>
                            </li>

                        </ul>

                        <ul class="nav navbar-nav pull-right" style="padding: 0;">
                            <li style="text-align: center;">
                                <span style="font-size: 15px; font-weight: bold; padding: 0; margin: 0;">
                                    <span class="tel" style="font-size: 25px">8 800 700 34 52</span><br>
                                    <?= Html::a('заказать обратный звонок', '#', ['data-target' => '#' . $callbackWidget->id, 'data-toggle' => 'modal', 'style' => 'border-bottom: 1px dashed']); ?></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <?= $content ?>

    <footer class="dark-bg">

        <div class="footer-bottom">
            <div class="container inner">
                <p class="pull-left"></p>
                <ul class="footer-menu pull-right">

                </ul>
            </div>
        </div>
    </footer>

    <script src="<?= Yii::getAlias('@web') ?>/js/jquery.min.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/jquery.easing.1.3.min.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/jquery.form.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/jquery.validate.min.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/bootstrap.min.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/bootstrap-hover-dropdown.min.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/skrollr.min.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/skrollr.stylesheets.min.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/waypoints.min.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/waypoints-sticky.min.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/owl.carousel.min.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/jquery.isotope.min.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/jquery.easytabs.min.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/google.maps.api.v3.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/viewport-units-buggyfill.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/scripts.js"></script>

    <script src="<?= Yii::getAlias('@web') ?>/switchstylesheet/switchstylesheet.js"></script>

    <script>
        $(document).ready(function () {
            $(".changecolor").switchstylesheet({ seperator: "color"});
        });
    </script>
    <!-- For demo purposes – can be removed on production : End -->
    </body>
    </html>
<?php $this->endPage() ?>